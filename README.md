## A Simple command for incremental changes deployment

### Generate Archive

php wwwsync.php zip --tickets=".\test\skeleton\dummy_root" --sync=".\test\src_wwwroot"

### Extract Archive Changes to Web Root

php wwwsync.php unzip --archive=".\wwwroot_yyyymmddhhiiss.zip" --dest=".\test\dest_wwwroot"

### Rollback Changes From a Given Archive (Use at your own risk)

php wwwsync.php rollback --changes=".\wwwroot_yyyymmddhhiiss.zip" --dest=".\test\dest_wwwroot"