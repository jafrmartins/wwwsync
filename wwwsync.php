<?php

$rc = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "wwwsyncrc.json");
$rc = json_decode($rc, true);

define('FOLDERNAME_WHITELIST', $rc['folder_names']);
define('SQL_OPTIONS', [
    'sql_source' => $rc['sql_source'],
    'connection' => $rc['database_connection'] 
]);

define('DUBSYNC_COMMANDS', [
    "zip" => [
        'description' => 'Generates a new zip archive with the changes of each ticket in folder',
        'example' => 'php wwwsync.php zip --tickets="/path/to/incoming/changes/" --sync="/path/to/sync/files/from"'
    ],
    "unzip" => [
        'description' => 'Generates a new zip archive with the changes of each ticket in folder',
        'example' => 'php wwwsync.php unzip --archive="/path/to/generated/archive.zip" --dest="/path/to/dest/folder/to/sync/to"'
    ],
    "rollback" => [
        'description' => 'rollsback the changes applied by archive to filesystem',
        'example' => 'php wwwsync.php rollback --archive="/path/to/archive/changes/to/rollback/archive.zip" --dest="/path/to/dest/folder/where/changes/will/be/rolledback"'
    ],
    "sql" => [
        'description' => 'executes the sql commands stored in the sql folder on the configured database',
        'example' => 'php wwwsync.php sql --archive="/path/to/generated/archive.zip --down --up'
    ]
]);

function help($command=null, $commands=DUBSYNC_COMMANDS) {

    if($command && isset($commands[$command])) {
        $usage = PHP_EOL."USAGE dubrootsync.php $command <options>".PHP_EOL.PHP_EOL;
        foreach($commands[$command] as $k => $v) {
            $usage .= $k.": $v".PHP_EOL;
        } $usage .= PHP_EOL.PHP_EOL; die($usage);
    } else {
        $usage = PHP_EOL."USAGE: dubrootsync.php <command> <options>".PHP_EOL.PHP_EOL;
        foreach($commands as $command => $def) {
            $usage .= "\t".$command.PHP_EOL;
            foreach($commands[$command] as $k => $v) {
                $usage .= "\t\t".$k.": $v".PHP_EOL;
            }

            $usage .= PHP_EOL.PHP_EOL;
        } die($usage);
    }

}

function pdo_connect() {

    try {
        $conn = new PDO(SQL_OPTIONS['connection']);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "connected to ".SQL_OPTIONS['connection'].PHP_EOL;
        return $conn;
    } catch(PDOException $e) {
        echo "database error: " . $e->getMessage().PHP_EOL;
    }

}

function pdo_execute($connection, $query) {

    
}

function parse_argv(&$argv) {

    if( count($argv) < 1) { help(); }

    $command = $argv[1];
    if(!isset(DUBSYNC_COMMANDS[$command])) { help(); }
    
    $args = [];
    for ($i = 2; $i < count($argv); $i++) {
        if (preg_match('/^--([^=]+)=(.*)/', $argv[$i], $match)) {
            $args[$match[1]] = $match[2];
        } else if(preg_match('/^--(.*)/', $argv[$i], $match)) {
            $args[$match[1]] = true;
        }
    }
    
    if(!count(array_keys($args))) {
        help($command);
    }
    
    return [$command, $args];

}

function recursive_glob($pattern, $flags = 0) {
    $files = glob($pattern, $flags); 
    foreach (glob(dirname($pattern).DIRECTORY_SEPARATOR.'*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
        $files = array_merge(
            [],
            ...[$files, recursive_glob($dir . DIRECTORY_SEPARATOR . basename($pattern), $flags)]
        );
    }
    array_multisort($files);
    return $files;
}

function run_command_zip($args) {

    $ts = date("YmdHis");
    $src_path = realpath($args['tickets']);
    $files = recursive_glob($src_path.DIRECTORY_SEPARATOR."**");

    if(isset($args['sync'])) {

        $root_dir = null;
        $dest_path = realpath($args['sync']);

        foreach($files as $i => $file) {
    
            $srcfolder = str_replace($src_path, $dest_path, $file);
            if(is_dir($srcfolder) && in_array(basename($srcfolder), FOLDERNAME_WHITELIST)) {
                $root_dir = $srcfolder.DIRECTORY_SEPARATOR;
            } 
            
            if(is_dir($root_dir) && is_file($file) && in_array(basename($srcfolder), FOLDERNAME_WHITELIST)) {

                $rel_filepath = str_replace($src_path.DIRECTORY_SEPARATOR, "", $file);
                preg_match("/^(([\w|\d|_])+\\".DIRECTORY_SEPARATOR."([\w|\d|_])+)+/", $rel_filepath, $matches);
                $parts = explode(DIRECTORY_SEPARATOR, $matches[0]);
                $folder = array_shift($parts);

                $src_file = str_replace("/".$folder."\\".DIRECTORY_SEPARATOR."$/", '', $root_dir).str_replace($matches[0].DIRECTORY_SEPARATOR, "", $rel_filepath);
                
                copy($src_file, $file);

            }

        }
    
    }

    $zip = new ZipArchive;
    if ($zip->open("wwwroot_".$ts.".zip", ZipArchive::CREATE) === TRUE) {

        $files = recursive_glob($src_path.DIRECTORY_SEPARATOR."**");

        foreach($files as $i => $file) {
            
            $srcfolder = str_replace($src_path, $dest_path, $file);
            if(is_dir($srcfolder) && (in_array(basename($srcfolder), FOLDERNAME_WHITELIST))) {
                $root_dir = $srcfolder.DIRECTORY_SEPARATOR;
            }
            
            if(is_dir($root_dir) && is_file($file)) {

                $rel_filepath = str_replace($src_path.DIRECTORY_SEPARATOR, "", $file);
                preg_match("/^(([\w|\d|_])+\\".DIRECTORY_SEPARATOR."([\w|\d|_])+)+/", $rel_filepath, $matches);
                $parts = explode(DIRECTORY_SEPARATOR, $matches[0]);
                $folder = array_shift($parts);
                $ticket = array_pop($parts);

                $src_file = str_replace($matches[0], $folder, $rel_filepath);
                $zip->addFile($file, $ticket.DIRECTORY_SEPARATOR.$src_file);

            } 
            
            if(strpos($file, DIRECTORY_SEPARATOR.SQL_OPTIONS['sql_source'].DIRECTORY_SEPARATOR) && is_file($file)) {

                $rel_filepath = str_replace($src_path.DIRECTORY_SEPARATOR, "", $file);
                preg_match("/^(([\w|\d|_])+\\".DIRECTORY_SEPARATOR."([\w|\d|_])+)+/", $rel_filepath, $matches);
                $parts = explode(DIRECTORY_SEPARATOR, $matches[0]);
                $folder = array_shift($parts);
                $ticket = array_pop($parts);

                $src_file = str_replace($matches[0], $folder, $rel_filepath);
                $zip->addFile($file, $ticket.DIRECTORY_SEPARATOR.$src_file);

            }

        }

        $zip->close();

    }

}

function run_command_unzip($args) {

    $dest_root = realpath($args['dest']);

    $zip = new ZipArchive;
    $res = $zip->open($args['archive']);
    if ($res === TRUE) {
        
        $xzippath = sys_get_temp_dir().DIRECTORY_SEPARATOR.basename($args['archive']);
        $zip->extractTo($xzippath);
        $zip->close();

        $ts = basename($args['archive'], ".zip");
        $ts = explode("_", $ts);
        $ts = array_pop($ts);

        $tickets = scandir($xzippath);
        foreach($tickets as $i => $path) {

            if(in_array($path, [".", ".."])) {
                continue;
            }

            $ticket = $xzippath.DIRECTORY_SEPARATOR.$path;
            if(is_dir($ticket)) {

                $files = recursive_glob("$ticket".DIRECTORY_SEPARATOR."**");
                $root_dest = realpath($args['dest']);


                $root_dir = null;
                foreach($files as $j => $file) {

                    $dest_filepath = str_replace($ticket.DIRECTORY_SEPARATOR, "", $file);
                    $dest_path = $root_dest.DIRECTORY_SEPARATOR.$dest_filepath;
                    if(is_dir($dest_path) && in_array($dest_filepath, FOLDERNAME_WHITELIST)) {
                        $root_dir = $root_dest;
                    }       

                    $dest_file = str_replace($ticket.DIRECTORY_SEPARATOR, $root_dir.DIRECTORY_SEPARATOR, $file);
                    if(!file_exists($dest_file)) {

                        if(is_dir($file)) { @mkdir($dest_file, 0755, true); }
                        else if(is_file($file)) { copy($file, $dest_file); }

                        if(file_exists($dest_file)) {
                            echo "copied $file to $dest_file".PHP_EOL;
                        }

                    } else if(is_file($file)) {

                        $versions = glob("$dest_file\.v*");
                        if(!empty($versions)) {

                            $max_version = -1;
                            foreach($versions as $n => $version) {

                                $version = explode(".", basename($version));
                                $version = array_pop($version);

                                if('rolledback' == $version) {
                                    continue;
                                }

                                $version = preg_replace('/^v/', '', $version);
                                $version = intval($version);
                                if($version > $max_version) {
                                    $max_version = $version;
                                }
                            }
                            
                            $next_version = intval($max_version) + 1;
                            $moved_file = basename($file).".v$next_version";

                        } else {
                            $moved_file = basename($file).".v0";
                        }
                        
                        $dir = dirname($dest_file);
                        $backup_file = $dir.DIRECTORY_SEPARATOR.$moved_file;

                        copy($dest_file, $backup_file);
                        echo "original file '$dest_file' moved to '".str_replace($dest_root.DIRECTORY_SEPARATOR, "", $backup_file)."'".PHP_EOL;
                        
                        copy($file, $dest_file);
                        echo "incoming file '$file' copied to '".str_replace($dest_root.DIRECTORY_SEPARATOR, "", $dest_file)."'".PHP_EOL;
                        
                    }

                }

            }

        }

    } else { help("unzip"); }

}

function run_command_rollback($args) {

    $dest_root = realpath($args['dest']);

    $zip = new ZipArchive;
    $res = $zip->open($args['archive']);
    if ($res === TRUE) {
        
        $xzippath = sys_get_temp_dir().DIRECTORY_SEPARATOR.basename($args['archive']);
        $zip->extractTo($xzippath);
        $zip->close();

        $ts = basename($args['archive'], ".zip");
        $ts = explode("_", $ts);
        $ts = array_pop($ts);

        $tickets = array_reverse(scandir($xzippath));
        foreach($tickets as $i => $path) {

            if(in_array($path, [".", ".."])) {
                continue;
            }

            $ticket = $xzippath.DIRECTORY_SEPARATOR.$path;
            if(is_dir($ticket)) {

                $files = recursive_glob("$ticket".DIRECTORY_SEPARATOR."**");
                $root_dest = realpath($args['dest']);


                $root_dir = null;
                foreach($files as $j => $file) {

                    $dest_filepath = str_replace($ticket.DIRECTORY_SEPARATOR, "", $file);
                    $dest_path = $root_dest.DIRECTORY_SEPARATOR.$dest_filepath;
                    if(is_dir($dest_path) && in_array($dest_filepath, FOLDERNAME_WHITELIST)) {
                        $root_dir = $root_dest;
                    }       

                    $dest_file = str_replace($ticket.DIRECTORY_SEPARATOR, $root_dir.DIRECTORY_SEPARATOR, $file);
                    if(file_exists($dest_file) && is_file($file)) {

                        
                        $bu_file = null;
                        $versions = glob("$dest_file\.v*");
                        if(!empty($versions)) {

                            $max_version = -1;
                            foreach($versions as $n => $version) {

                                $version = explode(".", basename($version));
                                $version = array_pop($version);
                                
                                if('rolledback' == $version) {
                                    continue;
                                }

                                $version = preg_replace('/^v/', '', $version);
                                $version = intval($version);
                                if($version > $max_version) {
                                    $max_version = $version;
                                }
                            }

                            $moved_file = basename($file).".v$max_version";
                            $dir = dirname($dest_file);
                            $backup_file = $dir.DIRECTORY_SEPARATOR.$moved_file;

                            if(file_exists($backup_file)) {
                                copy($backup_file, $dest_file);
                                echo "rolledback file '$dest_file' to version '$moved_file'".PHP_EOL;
                                copy($backup_file, "$backup_file.rolledback");
                                unlink($backup_file);
                            }

                        } else {

                            echo "no previous version of '$dest_file' found!".PHP_EOL;

                        }
                        
                    }

                }

            }

        }

    } else { help("rollback"); }

}

function run_command_sql($args) {

    $connection = pdo_connect();


}

function run_command($argv) {
    
    list($command, $args) = parse_argv($argv);

    switch($command) {

        case "zip":

            if(isset($args['sync']) || isset($args['tickets'])) {

                if(isset($args['sync']) && !is_dir($args['sync'])) {
                    die('please specify --sync="/path/to/wwwroot/folder/to/sync/from"'.PHP_EOL.PHP_EOL);
                }

                if(!isset($args['tickets']) || !is_dir($args['tickets'])) {
                    die('please specify --tickets="/path/to/incoming/changes"'.PHP_EOL.PHP_EOL);
                }

                run_command_zip($args);

            } else { help("zip"); } break;

        case "unzip":

            if(isset($args['archive']) && isset($args['dest'])) {

                if(!is_file($args['archive'])) {
                    die('please specify --archive="/path/to/the/zip/file/"'.PHP_EOL.PHP_EOL);
                }

                if(!is_dir($args['dest'])) {
                    die('please specify --dest="/path/to/the/wwwroot/folder/to/sync/changes/to"'.PHP_EOL.PHP_EOL);
                }
                
                run_command_unzip($args);

            } else { help("unzip"); } break;


        case "rollback":

            if(isset($args['archive']) && isset($args['dest'])) {

                if(!is_file($args['archive'])) {
                    die('please specify --archive="/path/to/the/zip/file/"'.PHP_EOL.PHP_EOL);
                }

                if(!is_dir($args['dest'])) {
                    die('please specify --dest="/path/to/the/wwwroot/folder/to/rollback/changes"'.PHP_EOL.PHP_EOL);
                }
                
                run_command_rollback($args);

            } else { help("rollback"); } break;


        case "sql":

            if(isset($args['archive']) && (isset($args['up']) || isset($args['down']))) {

                if(!is_file($args['archive'])) {
                    die('please specify --archive="/path/to/the/zip/file/"'.PHP_EOL.PHP_EOL);
                }

                run_command_sql($args);

            } else { help("sql"); } break;

    }


} run_command($argv);